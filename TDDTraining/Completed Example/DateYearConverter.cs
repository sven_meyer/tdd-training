﻿namespace TDDTraining.Completed_Example
{
  /// <summary>
  /// Problem Setting: 
  /// From other parts of the program we are given the number of days since the beginning of the year 2006. 
  /// Our DateYearConverter is supposed to calculate and return the current year. 
  /// </summary>
  public static class DateYearConverter
  {
    private const int NUMBER_OF_DAYS_IN_ONE_YEAR = 365;
    private const int NUMBER_OF_DAYS_LEAP_YEAR = 366;

    public static int GetYearFromDays(int numberOfDays)
    {
      var year = 2006;
      var numberOfDaysThisYear = NUMBER_OF_DAYS_IN_ONE_YEAR;
      while (numberOfDays > numberOfDaysThisYear)
      {
        year += 1;
        numberOfDays -= numberOfDaysThisYear;
        numberOfDaysThisYear = NumberOfDaysNextYear(year);
      }
      return year; 
    }

    private static int NumberOfDaysNextYear(int year)
    {
      return IsLeapYear(year) ? NUMBER_OF_DAYS_LEAP_YEAR : NUMBER_OF_DAYS_IN_ONE_YEAR;
    }

    private static bool IsLeapYear(int year)
    {
      return System.DateTime.IsLeapYear(year);
    }
  }
}