﻿namespace TDDTraining.Completed_Example
{
  using NUnit.Framework;

  [TestFixture]
  public class DateYearConverterTest
  {
    private const int NUMBER_OF_DAYS_IN_ONE_YEAR = 365;
    private const int NUMBER_OF_DAYS_LEAP_YEAR = 366;

    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void TestFirstDayOf2006()
    {
      AssertCorrectYear(2006, 1);
    }

    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void TestLastDayOf2006()
    {
      AssertCorrectYear(2006, NUMBER_OF_DAYS_IN_ONE_YEAR);
    }

    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void TestFirstDayOf2007()
    {
      AssertCorrectYear(2007, NUMBER_OF_DAYS_IN_ONE_YEAR + 1);
    }

    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void TestLastDayOf2007()
    {
      AssertCorrectYear(2007, NUMBER_OF_DAYS_IN_ONE_YEAR * 2);
    }

    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void TestFirstDayOf2008()
    {
      AssertCorrectYear(2008, NUMBER_OF_DAYS_IN_ONE_YEAR * 2 + 1 );
    }

    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void LastDayOf2008_LeapYear()
    {
      AssertCorrectYear(2008, NUMBER_OF_DAYS_IN_ONE_YEAR * 2 + NUMBER_OF_DAYS_LEAP_YEAR);
    }

    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void FirstDayOf2009()
    {
      AssertCorrectYear(2009, NUMBER_OF_DAYS_IN_ONE_YEAR * 2 + NUMBER_OF_DAYS_LEAP_YEAR + 1);
    }

    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void LastDayOf2009()
    {
      AssertCorrectYear(2009, NUMBER_OF_DAYS_IN_ONE_YEAR * 3 + NUMBER_OF_DAYS_LEAP_YEAR);
    }

    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void FirstDayOf2010()
    {
      AssertCorrectYear(2010, NUMBER_OF_DAYS_IN_ONE_YEAR  * 3 + NUMBER_OF_DAYS_LEAP_YEAR + 1);
    }

    private static void AssertCorrectYear(int expectedYear, int numberOfDays)
    {
      var year = DateYearConverter.GetYearFromDays(numberOfDays);
      Assert.AreEqual(expectedYear, year);
    }
  }
}
