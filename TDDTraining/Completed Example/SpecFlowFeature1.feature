﻿Feature: DateTimeConverter

Scenario Outline: Check the FIRST day of every year
	Given the system returns <days> day(s) since the beginning of 2006
	Then the DateTimeConverter shall return the year <year> as the result.

Examples: 
 | year | year type | number of days | days |
 | 2006 | Normal    | 365            | 1    |
 | 2007 | Normal    | 365            | 366  |
 | 2008 | Leap      | 366            | 732  |
 | 2009 | Normal    | 365            | 1097 |
 | 2010 | Normal    | 365            | 1462 |
 | 2011 | Normal    | 365            | 1827 |
 | 2012 | Leap      | 366            | 2193 |
 | 2013 | Normal    | 365            | 2558 |
 | 2014 | Normal    | 365            | 2923 |
 | 2015 | Normal    | 365            | 3288 |
 | 2016 | Leap      | 366            | 3654 |
 | 2017 | Normal    | 365            | 4019 |
 | 2018 | Normal    | 365            | 4384 |
 | 2019 | Normal    | 365            | 4749 |
 | 2020 | Leap      | 366            | 5115 |
 | 2021 | Normal    | 365            | 5480 |
 | 2022 | Normal    | 365            | 5845 |
 | 2023 | Normal    | 365            | 6210 |
 | 2024 | Leap      | 366            | 6576 |
 | 2025 | Normal    | 365            | 6941 |
 | 2026 | Normal    | 365            | 7306 |
 | 2027 | Normal    | 365            | 7671 |
 | 2028 | Leap      | 366            | 8037 |
 | 2029 | Normal    | 365            | 8402 |
 | 2030 | Normal    | 365            | 8767 |
 | 2031 | Normal    | 365            | 9132 |
 | 2032 | Leap      | 366            | 9498 |

 Scenario Outline: Check the LAST day of every year
	Given the system returns <days> day(s) since the beginning of 2006
	Then the DateTimeConverter shall return the year <year> as the result.

Examples: 
 | year | year type | number of days | days |
 | 2006 | Normal    | 365            | 365  |
 | 2007 | Normal    | 365            | 730  |
 | 2008 | Leap      | 366            | 1096 |
 | 2009 | Normal    | 365            | 1461 |
 | 2010 | Normal    | 365            | 1826 |
 | 2011 | Normal    | 365            | 2191 |
 | 2012 | Leap      | 366            | 2557 |
 | 2013 | Normal    | 365            | 2922 |
 | 2014 | Normal    | 365            | 3287 |
 | 2015 | Normal    | 365            | 3652 |
 | 2016 | Leap      | 366            | 4018 |
 | 2017 | Normal    | 365            | 4383 |
 | 2018 | Normal    | 365            | 4748 |
 | 2019 | Normal    | 365            | 5113 |
 | 2020 | Leap      | 366            | 5479 |
 | 2021 | Normal    | 365            | 5844 |
 | 2022 | Normal    | 365            | 6209 |
 | 2023 | Normal    | 365            | 6574 |
 | 2024 | Leap      | 366            | 6940 |
 | 2025 | Normal    | 365            | 7305 |
 | 2026 | Normal    | 365            | 7670 |
 | 2027 | Normal    | 365            | 8035 |
 | 2028 | Leap      | 366            | 8401 |
 | 2029 | Normal    | 365            | 8766 |
 | 2030 | Normal    | 365            | 9131 |
 | 2031 | Normal    | 365            | 9496 |
 | 2032 | Leap      | 366            | 9862 |
