﻿using TechTalk.SpecFlow;

namespace TDDTraining.Completed_Example
{
  using NUnit.Framework;

  [Binding]
  public class DateTimeConverterSteps
  {
    [Given(@"the system returns (.*) day\(s\) since the beginning of 2006")]
    public void GivenTheSystemReturnsDaysSinceTheBeginningOf(int givenDays)
    {
      Year = DateYearConverter.GetYearFromDays(givenDays);
    }

    private int Year { get; set; }

    [Then(@"the DateTimeConverter shall return the year (.*) as the result\.")]
    public void ThenTheDateTimeConverterShallReturnTheYearAsTheResult_(int expectedYear)
    {
      Assert.AreEqual(expectedYear, Year);
    }
  }
}
