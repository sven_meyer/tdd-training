﻿using NUnit.Framework;

namespace TDDTraining
{
  [TestFixture]
  public class DateYearConverterTest
  {
    /// <summary>
    /// See method name.
    /// </summary>
    [Test]
    public void TestFirstDayOf2006()
    {
      Assert.AreEqual(1, 2);
    }
  }
}
